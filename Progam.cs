﻿using System;

namespace maths19
{
    class Program
    {
        static void Main(string[] args)
        {
                //Start the program with clear();
                Console.Clear();

            var num1 = 5;
            var num2 = 4;
            var num3 = 5;
            var num4 = 2;
            var num5 = 3;
            var num6 = 4;
            var num7 = 9;
            var num8 = 7;

            //Exercise #18 A & B
            Console.WriteLine($"The value of num1 = {num1}");
            Console.WriteLine($"The value of num2 = {num2}");
            Console.WriteLine($"The value of num3 = {num3}");
            Console.WriteLine($"The value of num4 = {num4}");
            Console.WriteLine($"The value of num5 = {num5}");
            Console.WriteLine($"The value of num6 = {num6}");
            Console.WriteLine($"The value of num7 = {num7}");
            Console.WriteLine($"The value of num8 = {num8}");
           
            //Exercise #18 B
            Console.WriteLine($"{num1 + num2}");
            Console.WriteLine($"{num3 + num4}");
            Console.WriteLine($"{num5 + num3}");

            //Exercise #18 C
            Console.WriteLine($"{num1} = {num4} + {num5}");
            Console.WriteLine($"{num2} = {num4} + {num4}");
            Console.WriteLine($"{num7} = {num1} + {num2}");
            Console.WriteLine($"{num8} = {num6} + {num5}");
        
           //Exercuse #18 D
           Console.WriteLine("I am thinking of a number...");

            var num22 = int.Parse(Console.ReadLine());

            Console.WriteLine($"The number you typed in multiplied by 3 is {num22 * 3}");
        }
    }
}